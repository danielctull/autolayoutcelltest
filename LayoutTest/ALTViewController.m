//
//  ALTViewController.m
//  LayoutTest
//
//  Created by Pete Callaway on 25/09/2013.
//  Copyright (c) 2013 Dative Studios. All rights reserved.
//

#import "ALTViewController.h"

#import "ALTViewCell.h"

@interface ALTViewController ()

@property (nonatomic, strong) NSArray *sentences;
@property (nonatomic, strong) ALTViewCell *sizingCell;

@end


@implementation ALTViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sentences = @[@"Sentence one",
                       @"Sentence two",
                       @"",
                       @"Sentence three and a bit",
                       @"Sentence four a bit and some more",
                       @"",
                       @"Sentence five with gusto and passion",
                       @"Sentence six and a shake of a cats tail",
                       @"",
                       @"Sentence seven is dedicated to the seventh version of iOS",
                       @"Sentence eight is really lucky in China",
                       @"",
                       @"Sentence nine means I've got bored now."];
}


#pragma mark UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.sentences.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ALTViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ALTViewCell reuseIdentifier] forIndexPath:indexPath];
    [cell configureWithSentence:self.sentences[indexPath.row]];
    
    return cell;
}


#pragma mark UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.sizingCell == nil) {
        self.sizingCell = [tableView dequeueReusableCellWithIdentifier:[ALTViewCell reuseIdentifier]];
    }
    
    [self.sizingCell configureWithSentence:self.sentences[indexPath.row]];
    
    return [self.sizingCell.contentView systemLayoutSizeFittingSize:CGSizeMake(tableView.bounds.size.width, 9999)].height + 1;
}

@end
