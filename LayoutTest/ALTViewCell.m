//
//  ALTViewCell.m
//  LayoutTest
//
//  Created by Pete Callaway on 25/09/2013.
//  Copyright (c) 2013 Dative Studios. All rights reserved.
//

#import "ALTViewCell.h"

@interface ALTViewCell ()

@property (nonatomic, weak) IBOutlet UILabel *testLabel;

@end


@implementation ALTViewCell

+ (NSString*)reuseIdentifier {
    return NSStringFromClass([self class]);
}

- (void)configureWithSentence:(NSString*)sentence {
    self.testLabel.text = sentence;
}

@end
