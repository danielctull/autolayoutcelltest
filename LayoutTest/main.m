//
//  main.m
//  LayoutTest
//
//  Created by Pete Callaway on 25/09/2013.
//  Copyright (c) 2013 Dative Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ALTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ALTAppDelegate class]));
    }
}
